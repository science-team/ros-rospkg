ros-rospkg (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1
  * Rediff patches
  * Drop python3-mock dependency (Closes: #1069732)
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 24 Apr 2024 07:03:19 +0200

ros-rospkg (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 16 Jun 2023 19:31:23 +0200

ros-rospkg (1.4.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jochen Sprickerhof ]
  * Enable autopkgtest-pkg-pybuild
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 10 Dec 2022 12:52:52 +0100

ros-rospkg (1.4.0-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Drop gitlab-ci
  * Drop old Breaks/Replaces

  [ Timo Röhling ]
  * New upstream version 1.4.0
  * Add myself as uploader

 -- Timo Röhling <roehling@debian.org>  Fri, 25 Feb 2022 13:58:31 +0100

ros-rospkg (1.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.3.0
  * Bump Standards-Version to 4.6.0 (no changes)
  * Actually run unit test suite

 -- Timo Röhling <roehling@debian.org>  Thu, 02 Sep 2021 17:41:11 +0200

ros-rospkg (1.2.9-1) unstable; urgency=medium

  * New upstream version 1.2.9

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 15 Nov 2020 09:51:09 +0100

ros-rospkg (1.2.8-1) unstable; urgency=medium

  * New upstream version 1.2.8

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 27 Jun 2020 08:04:22 +0200

ros-rospkg (1.2.6-1) unstable; urgency=medium

  * New upstream version 1.2.6
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions
  * rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 07:55:16 +0200

ros-rospkg (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3
  * Drop patch applied upstream

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 05 Jan 2020 09:33:32 +0100

ros-rospkg (1.2.2-2) unstable; urgency=medium

  * Add fix for OS detection on Python 3.8

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 20 Dec 2019 10:41:58 +0100

ros-rospkg (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2 (Closes: #945868)
  * Drop upstreamed patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 14 Dec 2019 10:13:20 +0100

ros-rospkg (1.2.0-2) unstable; urgency=medium

  * Add patches for Python 3.8

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 24 Nov 2019 10:48:47 +0100

ros-rospkg (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 24 Nov 2019 08:26:17 +0100

ros-rospkg (1.1.10-2) unstable; urgency=medium

  * Drop Python 2 package (Closes: #938400)
  * Bump policy versions (no changes)
  * simplify d/watch

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 09:07:35 +0200

ros-rospkg (1.1.10-1) unstable; urgency=medium

  * New upstream version 1.1.10

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 17 Aug 2019 11:26:03 +0200

ros-rospkg (1.1.9-1) unstable; urgency=medium

  * New upstream version 1.1.9
  * rebase patches
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 28 Jul 2019 19:45:19 +0200

ros-rospkg (1.1.7-1) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * New upstream version 1.1.7
  * Fix ancient-python-version-field
  * Bump policy and debhelper versions
  * Rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 19 Aug 2018 08:37:19 +0200

ros-rospkg (1.1.4-1) unstable; urgency=medium

  * New upstream version 1.1.4

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 18 Oct 2017 08:05:20 +0200

ros-rospkg (1.1.3-1) unstable; urgency=medium

  * New upstream version 1.1.3

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 29 Sep 2017 11:35:39 +0200

ros-rospkg (1.1.2-1) unstable; urgency=medium

  * New upstream version 1.1.2

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 09 Sep 2017 16:46:52 +0200

ros-rospkg (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1
  * Rebase patches
  * Update policy and debhelper versions
  * Update watch file
  * Repackage using pybuild
  * Add python3-rospkg package

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 15 Jul 2017 08:33:14 +0200

ros-rospkg (1.0.41-1) unstable; urgency=medium

  * New upstream version 1.0.41

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 01 Oct 2016 17:25:53 +0200

ros-rospkg (1.0.40-1) unstable; urgency=medium

  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Fix unit tests
  * Update Vcs URLs
  * Update my email address
  * New upstream version 1.0.40

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 02 Sep 2016 11:43:51 +0200

ros-rospkg (1.0.39-1) unstable; urgency=medium

  * Add build-dep dh-python
  * Imported Upstream version 1.0.39

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jul 2016 23:14:57 +0200

ros-rospkg (1.0.38-1) unstable; urgency=medium

  * Initial release (Closes: #805261)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 18 Nov 2015 17:09:51 +0000
